<?php 

include 'header.php';

?><div class="container marketing ">
    <div class="row" >
    
    <?php
foreach($prod as $d){
    if($d['dispo'] == 1){
?>

        <div class="col-lg-4 bg-body-secondary rounded-5 border border-seccondary p-2 mb-2 border-opacity-25" >
            <img class="bd-placeholder-img rounded-circle" width="140" height="140" src="<?= '/cncB/img/'.$d['img']?>" alt="" srcset="">
            <h2 class="fw-normal"><?= $d['titre']?></h2>
            <span> <?= $d['prix']?> </span>
            <p><?= $d['txt']?></p>
            <audio controls muted src="<?= '/cncB/audio/'.$d['audio']?>"><a href="<?= '/cncB/audio/'.$d['audio']?>">Download audio</a></audio>
            <p><a class="btn btn-secondary" href="newCmd.php?id=<?= $d['id']?>">Ajouter au panier >></a></p>
        </div>
        <?php }} ?>
    </div>
</div>
<?php include 'footer.php';?>