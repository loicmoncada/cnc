drop database if exists music; 
create database music;
use music;
create table user(
    id int not null auto_increment primary key,
    nom varchar(255) not null,
    mail varchar(255) not null,
    tel varchar(255) not null
);
create table cmd(
    id int not null auto_increment primary key,
    id_user int not null,
    etat enum('panier','valide','pret','collecte') default 'panier'
);
create table ligne_cmd(
    id_prod int not null,
    id_cmd int not null
);
create table prod (
    id int not null auto_increment primary key,
    img varchar(2048) not null,
    titre varchar(255) not null,
    txt varchar(255) not null,
    audio varchar(2048) not null,
    prix float not null,
    dispo boolean
);
create table coord(
    id int not null,
    adr varchar(255) not null,
    mail varchar(255) not null,
    tel varchar(255) not null
);
drop USER if exists 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON music.* TO 'admin'@'127.0.0.1';