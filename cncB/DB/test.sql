use music;
insert into user(
    nom ,
    mail ,
    tel 
)
value(
    'rober',
    'rober@gmail.com',
    '0676655443'
);
insert into user(
    nom ,
    mail ,
    tel 
)
value(
    'giselle',
    'gigi@gmail.com',
    '0600112233'
);
insert into prod(
    img,
    titre,
    txt,
    audio ,
    prix,
    dispo 
)
value(
    '1.png',
    'Symba',
    'lorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipislorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipis',
    '4767.mp3',
    499.99,
    1
);
insert into prod(
    img,
    titre,
    txt,
    audio ,
    prix,
    dispo 
)
value(
    '2.jpg',
    'le tambourin quoi',
    'lorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipislorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipis',
    '13935.mp3',
    999.99,
    1
);
insert into prod(
    img,
    titre,
    txt,
    audio ,
    prix,
    dispo 
)
value(
    '3.jpg',
    'La flute du CDI',
    'lorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipislorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipis',
    '7636.mp3',
    19.99,
    1
);
insert into prod(
    img,
    titre,
    txt,
    audio ,
    prix,
    dispo 
)
value(
    '4.png',
    'guigui',
    'lorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipislorem ipsum dolor sit amet, consectetur adipis lorem ipsum dolor sit amet, consectetur adipis',
    '14601.mp3',
    1299.99,
    1
);
insert into coord(
    id,
    adr,
    mail,
    tel
)
value(
    1,
    '11 rue le general leclerc',
    'music@gmail.com',
    '0606050507'
);


