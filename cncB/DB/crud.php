<?php
include 'pdo.php';


function readAll($tab){
    global $pdo;
    $req = $pdo->query("SELECT*FROM $tab ;");
    return $req->fetchAll();
}
function readId($tab,$id){
    global $pdo;
    $req = $pdo->query("SELECT*FROM $tab where id=$id;");
    return $req->fetchAll();
}
function readP($com){
    global $pdo;
    $req = $pdo->query("
    select * from prod
    inner join ligne_cmd on prod.id=ligne_cmd.id_prod
    inner join cmd on cmd.id=ligne_cmd.id_cmd
    where cmd.id=$com;"
    );
    return $req->fetchAll();
}
function newCmd(){
    global $pdo;
    $req = $pdo->prepare('insert into cmd (id_user) value (0);');
    $req->execute([]);
}
function newLc($id_cmd,$id_prod){
    global $pdo;
    $req = $pdo->prepare('insert into ligne_cmd (id_cmd,id_prod) VALUES (?,?);');
    $req->execute([$id_cmd, $id_prod]);
}
function createU($nom,$mail,$tel){
    global $pdo;
    $req = $pdo->prepare('insert into user (nom,mail,tel) values (?,?,?);');
    $req->execute([$nom,$mail,$tel]); 
}
function plusDispo($id){
    global $pdo;
    $req = $pdo->prepare('update prod set dispo=0 where id=?;');
    $req->execute([$id]);
}
function dispoP($id){
    global $pdo;
    $req = $pdo->prepare('update prod set dispo=1 where id=?;');
    $req->execute([$id]);
}
function deleteL($id){
    global $pdo;
    $req = $pdo->prepare('delete from ligne_cmd where id_prod=?;');
    $req->execute([$id]);
}

function readPAll(){
    global $pdo;
    $req = $pdo->query("
    select * from prod
    inner join ligne_cmd on prod.id=ligne_cmd.id_prod
    inner join cmd on cmd.id=ligne_cmd.id_cmd;"
    );
    return $req->fetchAll();
}
function gigaTab(){
    global $pdo;
    $req = $pdo->query("select * from prod
    inner join ligne_cmd on prod.id=ligne_cmd.id_prod
    inner join cmd on cmd.id=ligne_cmd.id_cmd
    inner join user on user.id=cmd.id_user
    ");
    return  $req->fetchAll();
}
function mailC($tab,$m){
    foreach($tab as $v){
        if($v['mail']== $m){
            return $v['id'];
        }
    }
    return 1;
}
function etatU($id,$e){
    global $pdo;
    $req = $pdo->prepare("update cmd set etat=? where id=?;");
    $req->execute([$e, $id]);
}
function prodUp($img,$tit,$txt,$audio,$prix,$dispo,$id){
    global $pdo;
    $req =$pdo->prepare("update prod set img=?, titre=?, txt=?, audio=?, prix=?, dispo=? where id=?;");
    $req->execute([$img,$tit,$txt,$audio,$prix,$dispo,$id]);
}
function prodCr($img,$tit,$txt,$audio,$prix,$dispo){
    global $pdo;
    $req =$pdo->prepare("insert into prod (img,titre,txt,audio,prix,dispo) value (?,?,?,?,?,?);");
    $req->execute([$img,$tit,$txt,$audio,$prix,$dispo]);
}
function coordUp($adr,$mail,$tel,$id){
    global $pdo;
    $req =$pdo->prepare("update coord set adr=?, mail=?, tel=? where id=?;");
    $req->execute([$adr,$mail,$tel,$id]);
}
function prodUpi($tit,$txt,$audio,$prix,$dispo,$id){
    global $pdo;
    $req =$pdo->prepare("update prod set titre=?, txt=?, audio=?, prix=?, dispo=? where id=?;");
    $req->execute([$tit,$txt,$audio,$prix,$dispo,$id]);
}
function prodUpa($img,$tit,$txt,$prix,$dispo,$id){
    global $pdo;
    $req =$pdo->prepare("update prod set img=?, titre=?, txt=?, prix=?, dispo=? where id=?;");
    $req->execute([$img,$tit,$txt,$prix,$dispo,$id]);
}
// where id_user=$user