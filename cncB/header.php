<link rel="stylesheet" href="style/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<?php 
include 'data.php'; 
if(isset($_COOKIE['id_cmd'])){
    $id = $_COOKIE['id_cmd'];
}else{
    $id = 0;
}
$b = count(readP($id));
?>
<div class="px-3 py-2 text-bg-dark">
      <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
                <img class='bi me-2 headerimg ' src="/cncB/img/batterie.png" alt="" srcset="">
            </a>
            <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                
                <li>
                    <a href="/cncB/index.php"" class="nav-link text-white">
                        <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                            <use xlink:href="#home"></use>
                        </svg>
                    <h2>Musique&CO</h2>
                </a>
            
            </li>
            <li>
                <a href="/cncB/boutique.php" class="nav-link text-white">
                    <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                        <use xlink:href="#speedometer2"></use>
                    </svg>
                    Boutique
                </a>
            </li>
            <li>
                <a href="/cncB/pannier.php" class="nav-link text-white">
                    <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                        <use xlink:href="#table"></use>
                    </svg>
                    Panier - <?= $b ?>
                </a>
            </li>
            <li>
                <a href="/cncB/reset.php" class="nav-link text-white">
                    <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                        <use xlink:href="#grid"></use>
                    </svg>
                    Cookies
                </a>
            </li>
            <li>
                <a href="/cncB/admin" class="nav-link text-white">
                    <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                        <use xlink:href="#people-circle"></use>
                    </svg>
                    Login
                </a>
            </li>
          </ul>
        </div>
      </div>
    </div>





<!-- 
<div class='d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start'>
    <div>
        <img class='bi me-2' src="/cncB/img/batterie.png" alt="" srcset="">
    </div>
    <div>

        <div>
            <a class='d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none' href="/cncB/index.php">
                Musique&Co
            </a> 
        </div>
    </div>
    <div>
        <a class='d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none' href="/cncB/boutique.php">
           <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>Boutique</ul> 
        </a>
    </div>
    <div>
        <a class='d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none' href="/cncB/pannier.php">
            Pannier  <div class='notif'><?= $b ?></div>
        </a>
    </div>
    <div>
        <form action="reset.php" method="get">
            <input type="submit" value="Clear Cookie" >
        </form>
    </div>
    <div>
        <a class='d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none' href="/cncB/admin">
            Se connecter
        </a>
     
    </div>
</div> -->
