

    <div class="container">
  <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted"><?= $coord[0]['adr']?></a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted"><?= $coord[0]['mail']?></a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted"><?= $coord[0]['tel']?></a></li>
      

    </ul>
    <p class="text-center text-muted">Mention legales </p>
    <p class="text-center text-muted">A propos</p>

    <p class="text-center text-muted">© 2023 Music&Co, Inc</p>
  </footer>
</div>