<?php 
include 'header.php';


$id = $_COOKIE['id_cmd'];
$commande = readP($id);


?>
<section>
    <div class='pannier'>
        <?php foreach($commande as $v){?>
        <div class="pcard rounded-5 rounded-top-0">
            <div>
                <img src="<?= '/cncB/img/'.$v['img']?>" alt="">
            </div>
            <div>
                <h2>
                    <?= $v['titre']?>
                </h2>
            </div>
            <div>
                <span>
                    <?= $v['prix']?>
                </span>
            </div>
            <div>
                <a href="ctrl/deleteP.php?id=<?= $v['id_prod'] ?>">
                    X
                </a>
            </div>
        </div>
        <?php } 
        $totale=0;
        foreach($commande as $v){
            $totale += $v['prix'];
        }?>
        <div class='tot sticky'>
            Total = <?= $totale ?> €
        </div>
    </div>
    <div id='coord'>
      
        <form action="userXcmd.php" class='form' method="post">

            <label class="form-label" for="nom">Nom</label>
            <input class='form-control' type="text" name="nom" id=""> 

            <label  class="form-label" for="mail">Mail</label>
            <input  class='form-control' type="text" name="mail" id="">

            <label  class="form-label" for="tel">Téléphone</label>
            <input class='form-control' type="text" name="tel" id="">

            <input type="submit" class='btn btn-success' value="Valider">
        </form>
    </div>
</section>
<?php include 'footer.php';?>